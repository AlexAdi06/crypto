import tkinter as tk
from tkinter import *
from tkinter import filedialog
import string
import collections
import os


root = tk.Tk()
root.title("Cryptography AEA")
root.geometry("420x650")
root.configure(bg = "#434343")

size_label = tk.Label(root, text = "SIZE of the file in bytes")
size_label.pack(side = "top")


options = [x for x in [10000,20000,30000,40000,50000,60000,70000,80000,90000,100000,110000,120000,130000,140000,150000,160000,170000,180000,190000,200000,210000,220000,230000,240000,250000,260000,270000,280000,290000,300000,310000,320000,330000,340000,350000,360000,370000,380000,390000,400000,410000,420000,430000,440000,450000,460000,470000,480000,490000,500000]]
size = tk.IntVar(root)
size.set(options[0])
select_box = tk.OptionMenu(root, size, *options)
select_box.pack(side = "top")


def loadfile():
    global filename
    textfile = filedialog.askopenfilename(initialdir = "/Deskop/", title = "Select the file you want to encrypt", filetypes = (("Text", "*.txt"),("all files", "*.*")))
    if textfile!= None:
        filename = textfile


loadfileb = tk.Button(root, text= 'Load text file', padx = 50, pady = 2.5,fg = "black",bg = "white", command = loadfile)
loadfileb.pack()


filename = None


def lcg(modulus, a, b, seed):
    """Linear congruential generator."""
    seed = int(seed)
    a =int(a)
    b = int(b)
    seed = int(seed)
    modulus = int(modulus)
    while os.path.getsize(filename) < size.get():
        seed = (a * seed + b) % modulus
        with open (filename, 'a') as f:
            f.write(str(seed))

    print("It's done! Good job")
 

canvas = tk.Canvas(root, height = 300, width = 400, bg = "#818181")
canvas.pack()

Funct_label = tk.Label(root,borderwidth = 5, text = " Adaugati valorile LCG conform:  seed = (a * seed + b) % modulus ")
Funct_label.pack(side = "top")

a = Entry(root, width = 20, borderwidth = 5)
a.pack()
a.insert(0, "Enter the value for a:")

b = Entry(root, width = 20, borderwidth = 5)
b.pack()
b.insert(0, "Enter the value for b:")

seed = Entry(root, width = 25, borderwidth = 5)
seed.pack()
seed.insert(0, "Enter the value for seed:")

modulus = Entry(root, width = 25, borderwidth = 5)
modulus.pack()
modulus.insert(0, "Enter the value for modulus:")


#Functia care ia valorile introduse de user si face functia cu parametrii introdusi
def prng():
   va = a.get()
   vb = b.get()
   vseed = seed.get()
   vmodulus = modulus.get()
   mylabel  = Label(root, text = "You have generate pseudo random numbers in a file")
   mylabel.pack()
   lcg(vmodulus, va, vb, vseed)

def changetobinary():
    thisFile = filename
    base = os.path.splitext(thisFile)[0]
    os.rename(thisFile, base + ".bin")


ExecuteButton = Button(root, text = "Generate", command = prng, bg = "green")
ExecuteButton.pack()

BinaryButton = Button(root, text = "Convert to binary and save it",bg =  "#59BEBC", command = changetobinary)
BinaryButton.pack()

root.mainloop()